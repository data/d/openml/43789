# OpenML dataset: Used-cars-in-KSA

https://www.openml.org/d/43789

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

** This project display used cars in many different brands to pay. The number of used cars in the website is 1219 cars, I create data frame for them and each car has 9 features and the prediction of this data is car's price. 
I choice used cars in the project in order to use machine learning to predict car's price. Assign fair price for used car has a big issue depend on their features after usr it. So, I tried to collect as much as I can of features to give a good chance for ML to allocate a best price for car.
The kind of modeling that match with my data is supervised model because it includes label 'car_price' and use regression type because the target is number. Additionally, it can be used recommender system in this data .**

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43789) of an [OpenML dataset](https://www.openml.org/d/43789). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43789/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43789/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43789/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

